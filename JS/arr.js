const animal = ["crab", "dog", "frog"];

function ShowArr(arr, elem) {
  let out = "";
  for (let i = 0; i < arr.length; i++) {
    if (arr[i] !== undefined) {
      out += `<div><img src="images\\${arr[i]}.png"><span>${i}</span></div>`;
    }
  }
  document.querySelector(elem).innerHTML = out;
  document.querySelector(".out-1-source-length").innerHTML =
    "Long array:" + animal.length;
  document.querySelector(".out-1-source").innerHTML =
    "Array animals=[" + animal + "]";
}
ShowArr(animal, ".out-1-source-image");

function addToArray() {
  let index = +document.querySelector(".array-index").value;
  if (index > 50) {
    alert("Зачем так много");
  } else if (index < 0) {
    alert("Индекс больше");
  }
  animal[index] = document.querySelector(".array-element").value;
  ShowArr(animal, ".out-1-source-image");
}
document.querySelector(".add-to-array").onclick = addToArray;
//  reverse array

function ReverseArr(arr, elem) {
  let out = "";
  for (let i = arr.length - 1; i >= 0; i--) {
    if (arr[i] !== undefined) {
      out += `<div><img src="images\\${arr[i]}.png"><span>${i}</span></div>`;
    }
  }
  document.querySelector(elem).innerHTML = out;
}

function AddReverse() {
  let index = +document.querySelector(".array-index").value;
  if (index > 50) {
    alert("Зачем так много");
  } else if (index < 0) {
    alert("Индекс больше");
  }
  animal[index] = document.querySelector(".array-element").value;
  ReverseArr(animal, ".out-1-source-image");
}

document.querySelector(".reverse-array").onclick = AddReverse;
